use std::ops;
use std::fmt;
use std::f32;
use matrix::Matrix;
use serde_derive::{Serialize,Deserialize};

use crate::vec3;
use crate::vec4;
use crate::vector3::Vec3;
use crate::vector4::Vec4;


#[repr(C)]
#[derive(Copy, Clone,Debug,Serialize,Deserialize,PartialEq)]
pub struct Vec2{
  pub x:f32,
  pub y:f32
}
#[macro_export]
macro_rules! vec2 {
   ($x:expr,$y:expr) => {
      Vec2{x:$x,y:$y}
  };
}

impl fmt::Display for Vec2 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "x: {}, y: {}", self.x, self.y)
    }
}

impl From<(f32,f32)> for Vec2 {
  fn from(value: (f32,f32)) -> Self {
      Self{x:value.0,y:value.1}
  }
}

impl Vec2{ 
  pub fn is_zero(&self)->bool{
    self.x==0.0 && self.y==0.0
  }

  pub fn to_vec4(self)->Vec4{
    vec4!(
      self.x,
      self.y,
      0.0,
      1.0
    )
  }
  pub fn to_vec3(self)->Vec3{
    vec3!(
      self.x,
      self.y,
      0.0
    )
  }

  pub fn add(a:&Vec2,b:&Vec2)->Vec2{
    vec2!(
      a.x + b.x,
      a.y + b.y
    )
  }

  pub fn sub(a:&Vec2,b:&Vec2)->Vec2{
    vec2!(
      a.x - b.x,
      a.y - b.y
    )
  }

  pub fn scalar(a:&Vec2,b:f32)->Vec2{
    vec2!(
      a.x * b,
      a.y * b
    )
  }

  pub fn dot(a:&Vec2,b:&Vec2)->f32{
    a.x * b.x + a.y * b.y
  }
  pub fn length(&self)->f32{
    (self.x * self.x + self.y * self.y).sqrt()
  }
  pub fn normalize(&mut self){
    let l=self.length();
    if l!=0.0 {
      self.x/=l;
      self.y/=l;
    } 
  }
  pub fn normalized(self)->Vec2{
    let l=self.length();
    if l!=0.0 {
      vec2!(self.x/l,self.y/l)
    }else{
      self
    }
  }
  pub fn project(self, v:&Vec2) ->Vec2{
    let b :f32= v.x * v.x + v.y * v.y ;
    if b != 0.0 {
      let d :f32= (self.x * v.x + self.y * v.y) / b;
      vec2!(v.x * d, v.y * d)
    } else {
      vec2!(0.0,0.0)
    }
  }
  pub fn mul(&self,m:&Matrix)->Vec2{
    vec2!(
      self.x*m.0[0][0] + self.y*m.0[1][0] + m.0[2][0]  + m.0[3][0],
      self.x*m.0[0][1] + self.y*m.0[1][1] + m.0[2][1]  + m.0[3][1]
    )
  }
}


impl ops::Add for Vec2{
  type Output = Vec2;
  fn add(self,other:Vec2)->Vec2{
    Self::add(&self,&other)
  }
}

impl ops::AddAssign for Vec2 {
  fn add_assign(&mut self, other: Vec2) {
      *self = Self::add(&self,&other)
  }
}

impl ops::Sub for Vec2{
  type Output = Vec2;
  fn sub(self,other:Self)->Vec2{
    Self::sub(&self,&other)
  }
}

impl ops::SubAssign for Vec2 {
  fn sub_assign(&mut self, other: Vec2) {
      *self = Self::sub(&self,&other)
  }
}

impl ops::Mul<Matrix> for Vec2{
  type Output = Vec2;
  fn mul(self,other:Matrix)->Vec2{
    Self::mul(&self,&other)
  }
}
impl ops::MulAssign<Matrix> for Vec2{
  fn mul_assign(&mut self,other:Matrix){
    *self =Self::mul(&self,&other)
  }
}

impl ops::Mul<f32> for Vec2{
  type Output = Vec2;
  fn mul(self,other:f32)->Vec2{
    Self::scalar(&self, other)
  }
}

impl ops::MulAssign<f32> for Vec2 {
  fn mul_assign(&mut self, other: f32) {
      *self = Self::scalar(&self, other)
  }
}

impl ops::Div<f32> for Vec2{
  type Output = Vec2;
  fn div(self,other:f32)->Vec2{
    Self::scalar(&self, 1.0/other)
  }
}

impl ops::DivAssign<f32> for Vec2 {
  fn div_assign(&mut self, other: f32) {
      *self = Self::scalar(&self, 1.0/other)
  }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        assert_eq!(Vec2::add(&vec2!(1.0,1.0), &vec2!(2.0,2.0)), vec2!(3.0,3.0));
    }
    #[test]
    fn test_add_op() {
        assert_eq!(vec2!(1.0,1.0)+vec2!(2.0,2.0), vec2!(3.0,3.0));
    }
    #[test]
    fn test_sub() {
        assert_eq!(Vec2::sub(&vec2!(1.0,1.0), &vec2!(2.0,2.0)), vec2!(-1.0,-1.0));
    }
  
    #[test]
    fn test_scalar() {
        assert_eq!(Vec2::scalar(&vec2!(1.0,1.0), 3.0), vec2!(3.0,3.0));
    }
    #[test]
    fn test_scalar_op() {
        assert_eq!(vec2!(1.0,1.0)* 3.0, vec2!(3.0,3.0));
    }
    #[test]
    fn test_dot() {
        assert_eq!(Vec2::dot(&vec2!(1.0,1.0), &vec2!(1.0,1.0)), 2.0);
    }
    #[test]
    fn test_length() {
        assert_eq!(vec2!(3.0,4.0).length(), 5.0);
    }
    #[test]
    fn test_normalize() {
        let mut v=vec2!(2.0,2.0);
        v.normalize();
        assert!(v.length()>0.99999991 && v.length() < 1.0000001 )
    }
    #[test]
    fn test_normalized() {
        let v=vec2!(2.0,2.0).normalized();
        assert!(v.length()>0.99999991 && v.length() < 1.0000001 )
    }
    #[test]
    fn test_is_zero() {
      assert!(vec2!(0.0,0.0).is_zero());
    }
}