use std::ops;
use std::fmt;
use std::f32;
use matrix::Matrix;
use serde_derive::{Serialize,Deserialize};

#[repr(C)]
#[derive(Copy, Clone,Debug,Serialize,Deserialize,PartialEq)]
pub struct Vec4{
  pub x:f32,
  pub y:f32,
  pub z:f32,
  pub w:f32
}
#[macro_export]
macro_rules! vec4 {
  ($x:expr,$y:expr,$z:expr,$w:expr) => {
    Vec4{x:$x,y:$y,z:$z,w:$w}
 };
}

impl fmt::Display for Vec4 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "x: {}, y: {}, z: {}, w: {}", self.x, self.y, self.z, self.w)
    }
}

impl From<(f32,f32,f32,f32)> for Vec4 {
    fn from(value: (f32,f32,f32,f32)) -> Self {
        Self{x:value.0,y:value.1,z:value.2,w:value.3}
    }
}

impl Vec4{ 
  pub fn add(a:&Vec4,b:&Vec4)->Vec4{
    vec4!(
      a.x + b.x,
      a.y + b.y,
      a.z + b.z,
      a.w + b.w
    )
  }

  pub fn sub(a:&Vec4,b:&Vec4)->Vec4{
    vec4!(
      a.x - b.x,
      a.y - b.y,
      a.z - b.z,
      a.w - b.w
    )
  }

  pub fn scalar(a:&Vec4,b:f32)->Vec4{
    vec4!(
      a.x * b,
      a.y * b,
      a.z * b,
      a.w * b
    )
  }
  pub fn cross(a:&Vec4,b:&Vec4)->Vec4{
    vec4!(
      a.y * b.z - a.z * b.y,
      a.z * b.x - a.x * b.z,
      a.x * b.y - a.y * b.x,
      1.0
    )
  }

  pub fn dot(a:&Vec4,b:&Vec4)->f32{
    a.x * b.x + a.y * b.y + a.z * b.z
  }

  pub fn length(self)->f32{
    (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
  }
  pub fn normalize(&mut self){
    let l=self.length();
    if l!=0.0 {
      self.x/=l;
      self.y/=l;
      self.z/=l;
    } 
  }
  pub fn normalized(self)->Vec4{
    let l=self.length();
    if l!=0.0 {
      vec4!(self.x/l,self.y/l,self.z/l,1.0)
    }else{
      self
    }
  }
  pub fn project(self, v:&Vec4) ->Vec4{
    let b :f32= v.x * v.x + v.y * v.y + v.z * v.z;
    if b != 0.0 {
      let d :f32= (self.x * v.x + self.y * v.y + self.z * v.z) / b;
      vec4!(v.x * d, v.y * d, v.z * d,1.0)
    } else {
      vec4!(0.0,0.0,0.0,1.0)
    }
  }
  pub fn mul(&self,m:&Matrix)->Vec4{
    //[x]   [m[0][0],m[0][1],m[0][2],m[0][3],
    //[y] x [m[1][0],m[1][1],m[1][2],m[1][3],
    //[z]   [m[2][0],m[2][1],m[2][2],m[2][3],
    //[w]   [m[3][0],m[3][1],m[3][2],m[3][3],

    vec4!(
      self.x*m.0[0][0] + self.y*m.0[1][0] + self.z*m.0[2][0]  +self.w* m.0[3][0],
      self.x*m.0[0][1] + self.y*m.0[1][1] + self.z*m.0[2][1]  +self.w* m.0[3][1],
      self.x*m.0[0][2] + self.y*m.0[1][2] + self.z*m.0[2][2]  +self.w* m.0[3][2],
      self.x*m.0[0][3] + self.y*m.0[1][3] + self.z*m.0[2][3]  +self.w* m.0[3][3]
    )
  }
  pub fn mul_self(&mut self,m:&Matrix){
   
    let  Vec4 {x,y,z,w} = *self;

    self.x= x*m.0[0][0] + y*m.0[1][0] + z*m.0[2][0]  + w* m.0[3][0];
    self.y= x*m.0[0][1] + y*m.0[1][1] + z*m.0[2][1]  + w* m.0[3][1];
    self.z= x*m.0[0][2] + y*m.0[1][2] + z*m.0[2][2]  + w* m.0[3][2];
    self.w= x*m.0[0][3] + y*m.0[1][3] + z*m.0[2][3]  + w* m.0[3][3];

  }
}

impl ops::Add for Vec4{
  type Output = Vec4;
  fn add(self,other:Vec4)->Vec4{
      vec4!(
        self.x + other.x,
        self.y + other.y,
        self.z + other.z,
        self.w + other.w
      )
  }
}

impl ops::AddAssign for Vec4 {
  fn add_assign(&mut self, other: Vec4) {
      *self = vec4!(
          self.x + other.x,
          self.y + other.y,
          self.z + other.z,
          self.w + other.w
      )
  }
}

impl ops::Sub for Vec4{
  type Output = Vec4;
  fn sub(self,other:Vec4)->Vec4{
      vec4!(
        self.x - other.x,
        self.y - other.y,
        self.z - other.z,
        self.w - other.w
      )
  }
}

impl ops::SubAssign for Vec4 {
  fn sub_assign(&mut self, other: Vec4) {
      *self = vec4!(
        self.x - other.x,
        self.y - other.y,
        self.z - other.z,
        self.w - other.w
      )
  }
}

impl ops::Mul<f32> for Vec4{
  type Output = Vec4;
  fn mul(self,other:f32)->Vec4{
      vec4!(
        self.x * other,
        self.y * other,
        self.z * other,
        self.w * other
      )
  }
}

impl ops::MulAssign<f32> for Vec4 {
  fn mul_assign(&mut self, other: f32) {
      *self = vec4!(
        self.x * other,
        self.y * other,
        self.z * other,
        self.w * other
      )
  }
}

impl ops::Div<f32> for Vec4{
  type Output = Vec4;
  fn div(self,other:f32)->Vec4{
      vec4!(
        self.x / other,
        self.y / other,
        self.z / other,
        self.w / other
      )
  }
}

impl ops::DivAssign<f32> for Vec4 {
  fn div_assign(&mut self, other: f32) {
      *self = vec4!(
        self.x / other,
        self.y / other,
        self.z / other,
        self.w / other
      )
  }
}

impl ops::Mul<Matrix> for Vec4{
  type Output = Vec4;
  fn mul(self,other:Matrix)->Vec4{
    Self::mul(&self,&other)
  }
}
impl ops::MulAssign<Matrix> for Vec4{
  fn mul_assign(&mut self,other:Matrix){
    *self =Self::mul(&self,&other)
  }
}

impl ops::Neg for Vec4{
  type Output = Vec4;
  fn neg(self) -> Self::Output {
      vec4!(-self.x,-self.y,-self.z,-self.w)
  }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        assert_eq!(Vec4::add(&vec4!(1.0,1.0,1.0,1.0), &vec4!(2.0,2.0,2.0,2.0)), vec4!(3.0,3.0,3.0,3.0));
    }
    #[test]
    fn test_add_op() {
        assert_eq!(vec4!(1.0,1.0,1.0,1.0)+vec4!(2.0,2.0,2.0,2.0), vec4!(3.0,3.0,3.0,3.0));
    }
    #[test]
    fn test_sub() {
        assert_eq!(Vec4::sub(&vec4!(1.0,1.0,1.0,1.0), &vec4!(2.0,2.0,2.0,2.0)), vec4!(-1.0,-1.0,-1.0,-1.0));
    }
    #[test]
    fn test_cross() {
        assert_eq!(Vec4::cross(&vec4!(1.0,0.0,0.0,1.0), &vec4!(0.0,1.0,0.0,1.0)), vec4!(0.0,0.0,1.0,1.0));
    }
    #[test]
    fn test_scalar() {
        assert_eq!(Vec4::scalar(&vec4!(1.0,2.0,3.0,4.0), 3.0), vec4!(3.0,6.0,9.0,12.0));
    }
    #[test]
    fn test_scalar_op() {
        assert_eq!(vec4!(1.0,2.0,3.0,4.0)* 3.0, vec4!(3.0,6.0,9.0,12.0));
    }
    #[test]
    fn test_dot() {
        //w component shouldn't count
        assert_eq!(Vec4::dot(&vec4!(1.0,1.0,1.0,2.0), &vec4!(1.0,1.0,1.0,1.0)), 3.0);
    }
    #[test]
    fn test_length() {
        //w component shouldn't count
        assert_eq!(vec4!(2.0,2.0,1.0,1.0).length(), 3.0);
    }
    #[test]
    fn test_normalize() {
        let mut v=vec4!(2.0,2.0,2.0,1.0);
        v.normalize();
        assert!(v.length()>0.99999991 && v.length() < 1.0000001 )
    }
    #[test]
    fn test_normalized() {
        let v=vec4!(2.0,2.0,2.0,1.0).normalized();
        assert!(v.length()>0.99999991 && v.length() < 1.0000001 )
    }
}