use matrix::Matrix;
use serde_derive::{Deserialize, Serialize};
use std::f32;
use std::fmt;
use std::ops;
use vector3::Vec3;
use vector4::Vec4;

use crate::angle::Rad;

#[repr(C)]
#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Quaternion {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub w: f32,
}

#[macro_export]
macro_rules! quat {
    ($x:expr,$y:expr,$z:expr,$w:expr) => {
        Quaternion {
            x: $x,
            y: $y,
            z: $z,
            w: $w,
        }
    };
}

impl fmt::Display for Quaternion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "x: {}, y: {}, z: {}, w: {}",
            self.x, self.y, self.z, self.w
        )
    }
}
impl ops::Mul for &Quaternion {
    type Output = Quaternion;
    fn mul(self, other: &Quaternion) -> Quaternion {
        Quaternion::multiply(self,other)
    }
}
impl ops::Mul for Quaternion {
    type Output = Quaternion;
    fn mul(self, other: Quaternion) -> Quaternion {
        Quaternion::multiply(&self,&other)
    }
}

impl ops::MulAssign for Quaternion {
    fn mul_assign(&mut self, other: Quaternion) {
        *self=Self::multiply(self,&other);
    }
}

impl Quaternion {
    pub fn identity() -> Quaternion {
        quat!(0.0, 0.0, 0.0, 1.0)
    }
    pub fn conjugate(self) -> Quaternion {
        quat!(-self.x, -self.y, -self.z, self.w)
    }

    pub fn magnitude(self) -> f32 {
        (self.x * self.x + self.y * self.y + self.z * self.z + self.w * self.w).sqrt()
    }

    pub fn normalize(&mut self) {
        let l = self.magnitude();
        if l != 0.0 {
            self.x /= l;
            self.y /= l;
            self.z /= l;
            self.w /= l;
        }
    }

    pub fn normalized(self) -> Quaternion {
        let l = self.magnitude();
        if l != 0.0 {
            quat!(self.x / l, self.y / l, self.z / l, 1.0)
        } else {
            self
        }
    }

    pub fn to_matrix(self) -> Matrix {
        let Quaternion { x, y, z, w } = self;
        let nq = x * x + y * y + z * z + w * w;
        let s = if nq > 0.0 { 2.0 / nq } else { 0.0 };
        let xs = x * s;
        let ys = y * s;
        let zs = z * s;
        let wx = w * xs;
        let wy = w * ys;
        let wz = w * zs;
        let xx = x * xs;
        let xy = x * ys;
        let xz = x * zs;
        let yy = y * ys;
        let yz = y * zs;
        let zz = z * zs;

        //Matrix([
        //    [1.0 - (yy + zz), xy + wz, xz - wy, 0.0],
        //    [xy - wz, 1.0 - (xx + zz), yz + wx, 0.0],
        //    [xz + wy, yz - wx, 1.0 - (xx + yy), 0.0],
        //    [0.0, 0.0, 0.0, 1.0],
        //])

        Matrix([
            [1.0 - (yy + zz), xy - wz        , xz + wy        , 0.0],
            [xy + wz        , 1.0 - (xx + zz), yz - wx        , 0.0],
            [xz - wy        , yz + wx        , 1.0 - (xx + yy), 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ])
    }

    pub fn from2vec4(a: &Vec4, b: &Vec4) -> Quaternion {
        let mut q = quat!(
            a.y * b.z - a.z * b.y,
            a.z * b.x - a.x * b.z,
            a.x * b.y - a.y * b.x,
            a.x * b.x + a.y * b.y + a.z * b.z
        );
        q.w += q.magnitude();
        q
    }

    pub fn from2vec3(a: &Vec3, b: &Vec3) -> Quaternion {
        let mut q = quat!(
            a.y * b.z - a.z * b.y,
            a.z * b.x - a.x * b.z,
            a.x * b.y - a.y * b.x,
            a.x * b.x + a.y * b.y + a.z * b.z
        );
        q.w += q.magnitude();
        q
    }

    pub fn from_vec4_angle(v: &Vec4, angle: Rad) -> Quaternion {
        let sin = (angle.0 * 0.5).sin();
        let mut q = quat!(v.x * sin, v.y * sin, v.z * sin, (angle.0 * 0.5).cos());
        //q.normalize();
        q
    }
    pub fn from_vec4_angle_ip(&mut self,v: &Vec4, angle: Rad){
        let sin = (angle.0 * 0.5).sin();
        //let mut q = quat!(v.x * sin, v.y * sin, v.z * sin, (angle * 0.5).cos());
        self.x=v.x*sin;
        self.y=v.y*sin;
        self.z=v.z*sin;
        self.w=(angle.0 * 0.5).cos();
        //self.normalize();
    }
    pub fn from_vec3_angle(v: &Vec3, angle: Rad) -> Quaternion {
        let sin = (angle.0 * 0.5).sin();
        let mut q = quat!(v.x * sin, v.y * sin, v.z * sin, (angle.0 * 0.5).cos());
        //q.normalize();
        q
    }
    pub fn from_vec3_angle_ip(&mut self,v: &Vec3, angle: Rad){
        let sin = (angle.0 * 0.5).sin();
        //let mut q = quat!(v.x * sin, v.y * sin, v.z * sin, (angle * 0.5).cos());
        self.x=v.x*sin;
        self.y=v.y*sin;
        self.z=v.z*sin;
        self.w=(angle.0 * 0.5).cos();
        //self.normalize();
    }
    pub fn to_vector_angle(self) -> (Vec3, Rad) {
        let q1 = if self.w > 1.0 {
            self.normalized()
        } else {
            self
        };
        let angle = 2.0 * q1.w.acos();
        let s = (1.0 - q1.w * q1.w).sqrt();
        let v = if s != 0.0 {
            vec3!(q1.x / s, q1.y / s, q1.z / s)
        } else {
            vec3!(q1.x, q1.y, q1.z)
        };
        (v, Rad(angle))
    }

    pub fn slerp(&mut self, qa: &Quaternion, qb: &Quaternion, t: f32) {
        let cos_half_theta = qa.w * qb.w + qa.x * qb.x + qa.y * qb.y + qa.z * qb.z;

        if cos_half_theta.abs() >= 1.0 {
            self.w = qa.w;
            self.x = qa.x;
            self.y = qa.y;
            self.z = qa.z;
        } else {
            let half_theta = cos_half_theta.acos();
            let sin_half_theta = (1.0 - cos_half_theta * cos_half_theta).sqrt();
            if sin_half_theta.abs() < 0.001 {
                self.w = qa.w * 0.5 + qb.w * 0.5;
                self.x = qa.x * 0.5 + qb.x * 0.5;
                self.y = qa.y * 0.5 + qb.y * 0.5;
                self.z = qa.z * 0.5 + qb.z * 0.5;
            } else {
                let ratio_a = ((1.0 - t) * half_theta).sin() / sin_half_theta;
                let ratio_b = (t * half_theta).sin() / sin_half_theta;
                self.w = qa.w * ratio_a + qb.w * ratio_b;
                self.x = qa.x * ratio_a + qb.x * ratio_b;
                self.y = qa.y * ratio_a + qb.y * ratio_b;
                self.z = qa.z * ratio_a + qb.z * ratio_b;
            }
        }
    }
    pub fn multiply(a:& Quaternion, b:& Quaternion) -> Quaternion {
        quat!(
            a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y,
            a.w * b.y + a.y * b.w + a.z * b.x - a.x * b.z,
            a.w * b.z + a.z * b.w + a.x * b.y - a.y * b.x,
            a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z
            //a.x * b.w + a.w * b.x + a.y * b.z - a.z * b.y,
            //a.y * b.w + a.w * b.y + a.z * b.x - a.x * b.z,
            //a.w * b.w + a.w * b.z + a.x * b.y - a.y * b.x,
            //a.z * b.w - a.x * b.x - a.y * b.y - a.z * b.z
        )
    }
    pub fn multiply_ip(&mut self, b:& Quaternion){
        let ax=self.x;
        let ay=self.y;
        let az=self.z;
        let aw=self.w;
        self.x=aw * b.x + ax * b.w + ay * b.z - az * b.y;
        self.y=aw * b.y + ay * b.w + az * b.x - ax * b.z;
        self.z=aw * b.z + az * b.w + ax * b.y - ay * b.x;
        self.w=aw * b.w - ax * b.x - ay * b.y - az * b.z;
        //self.x = ax * b.w + aw * b.x + ay * b.z - az * b.y;
        //self.y = ay * b.w + aw * b.y + az * b.x - ax * b.z;
        //self.z = aw * b.w + aw * b.z + ax * b.y - ay * b.x;
        //self.w = az * b.w - ax * b.x - ay * b.y - az * b.z;
        
    }
}
