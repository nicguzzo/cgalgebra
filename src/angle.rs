use std::f32;
use std::ops;
use serde_derive::{Serialize,Deserialize};

pub const PI: f32 = 3.141592653589793;
pub const PI_2: f32 = 1.570796326794896;
const TO_RADIANS: f32 = 0.0174532925199433;
const TO_DEGREES: f32 = 57.29577951308232;

#[repr(C)]
#[derive(Debug,Copy, Clone,Serialize,Deserialize,PartialEq,PartialOrd)]
pub struct Rad(pub f32);

#[repr(C)]
#[derive(Debug,Copy, Clone,Serialize,Deserialize,PartialEq,PartialOrd)]
pub struct Deg(pub f32);

impl From<Deg> for Rad {
    fn from(value: Deg) -> Self {
        Self(value.0 * TO_RADIANS)
    }
}

impl From<Rad> for Deg {
    fn from(value: Rad) -> Self {
        Self(value.0 * TO_DEGREES)
    }
}

impl ops::Add for Deg {
    type Output = Self;
    fn add(self, other: Deg) -> Deg {
        Deg(self.0 + other.0)
    }
}

impl ops::AddAssign for Deg {
    fn add_assign(&mut self, other: Deg) {
        *self = Deg(self.0 + other.0)
    }
}

impl ops::Sub for Deg {
    type Output = Self;
    fn sub(self, other: Self) -> Self::Output {
        Deg(self.0 - other.0)
    }
}

impl ops::SubAssign for Deg {
    fn sub_assign(&mut self, other: Deg) {
        *self = Deg(self.0 - other.0)
    }
}

impl ops::Mul<Deg> for Deg {
    type Output = Deg;
    fn mul(self, other: Deg) -> Deg {
        Deg(self.0 * other.0)
    }
}

impl ops::MulAssign<Deg> for Deg {
    fn mul_assign(&mut self, other: Deg) {
        *self = Deg(self.0 * other.0)
    }
}

impl ops::Mul<f32> for Deg {
    type Output = Deg;
    fn mul(self, other: f32) -> Deg {
        Deg(self.0 * other)
    }
}

impl ops::MulAssign<f32> for Deg {
    fn mul_assign(&mut self, other: f32) {
        *self = Deg(self.0 * other)
    }
}
impl ops::Div for Deg {
    type Output = Deg;
    fn div(self, other: Deg) -> Deg {
        Deg(self.0 / other.0)
    }
}
impl ops::Div<f32> for Deg {
    type Output = Deg;
    fn div(self, other: f32) -> Deg {
        Deg(self.0 / other)
    }
}

impl ops::DivAssign for Deg {
    fn div_assign(&mut self, other: Deg) {
        *self = Deg(self.0 / other.0)
    }
}

impl ops::DivAssign<f32> for Deg {
    fn div_assign(&mut self, other: f32) {
        *self = Deg(self.0 / other)
    }
}

impl ops::Neg for Deg {
    type Output = Deg;
    fn neg(self) -> Self::Output {
        Deg(-self.0)
    }
}

impl ops::Add for Rad {
    type Output = Self;
    fn add(self, other: Rad) -> Rad {
        Rad(self.0 + other.0)
    }
}

impl ops::AddAssign for Rad {
    fn add_assign(&mut self, other: Rad) {
        *self = Rad(self.0 + other.0)
    }
}

impl ops::Sub for Rad {
    type Output = Self;
    fn sub(self, other: Self) -> Self::Output {
        Rad(self.0 - other.0)
    }
}

impl ops::SubAssign for Rad {
    fn sub_assign(&mut self, other: Rad) {
        *self = Rad(self.0 - other.0)
    }
}

impl ops::Mul<Rad> for Rad {
    type Output = Rad;
    fn mul(self, other: Rad) -> Rad {
        Rad(self.0 * other.0)
    }
}

impl ops::MulAssign<Rad> for Rad {
    fn mul_assign(&mut self, other: Rad) {
        *self = Rad(self.0 * other.0)
    }
}

impl ops::Mul<f32> for Rad {
    type Output = Rad;
    fn mul(self, other: f32) -> Rad {
        Rad(self.0 * other)
    }
}

impl ops::MulAssign<f32> for Rad {
    fn mul_assign(&mut self, other: f32) {
        *self = Rad(self.0 * other)
    }
}

impl ops::Div for Rad {
    type Output = Rad;
    fn div(self, other: Rad) -> Rad {
        Rad(self.0 / other.0)
    }
}

impl ops::Div<f32> for Rad {
    type Output = Rad;
    fn div(self, other: f32) -> Rad {
        Rad(self.0 / other)
    }
}

impl ops::DivAssign for Rad {
    fn div_assign(&mut self, other: Rad) {
        *self = Rad(self.0 / other.0)
    }
}

impl ops::DivAssign<f32> for Rad {
    fn div_assign(&mut self, other: f32) {
        *self = Rad(self.0 / other)
    }
}

impl ops::Neg for Rad {
    type Output = Rad;
    fn neg(self) -> Self::Output {
        Rad(-self.0)
    }
}
