use std::ops;
use std::fmt;
use std::f32;
use matrix::Matrix;
use serde_derive::{Serialize,Deserialize};

use crate::vec4;
use crate::vector4::Vec4;

#[repr(C)]
#[derive(Copy, Clone,Debug,Serialize,Deserialize,PartialEq)]
pub struct Vec3{
  pub x:f32,
  pub y:f32,
  pub z:f32
}
#[macro_export]
macro_rules! vec3 {
   ($x:expr,$y:expr,$z:expr) => {
      Vec3{x:$x,y:$y,z:$z}      
  };
}

impl fmt::Display for Vec3 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "x: {}, y: {}, z: {}", self.x, self.y, self.z)
    }
}

impl From<(f32,f32,f32)> for Vec3 {
  fn from(value: (f32,f32,f32)) -> Self {
      Self{x:value.0,y:value.1,z:value.2}
  }
}

impl Vec3{ 
  pub fn is_zero(&self)->bool{
    self.x==0.0 && self.y==0.0 && self.z==0.0
  }

  pub fn to_vec4(self)->Vec4{
    vec4!(
      self.x,
      self.y,
      self.z,
      1.0
    )
  }

  pub fn add(a:&Vec3,b:&Vec3)->Vec3{
    vec3!(
      a.x + b.x,
      a.y + b.y,
      a.z + b.z
    )
  }

  pub fn sub(a:&Vec3,b:&Vec3)->Vec3{
    vec3!(
      a.x - b.x,
      a.y - b.y,
      a.z - b.z
    )
  }

  pub fn scalar(a:&Vec3,b:f32)->Vec3{
    vec3!(
      a.x * b,
      a.y * b,
      a.z * b
    )
  }
  pub fn cross(a:&Vec3,b:&Vec3)->Vec3{
    vec3!(
      a.y * b.z - a.z * b.y,
      a.z * b.x - a.x * b.z,
      a.x * b.y - a.y * b.x
    )
  }

  pub fn dot(a:&Vec3,b:&Vec3)->f32{
    a.x * b.x + a.y * b.y + a.z * b.z
  }
  pub fn length(&self)->f32{
    (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
  }
  pub fn normalize(&mut self){
    let l=self.length();
    if l!=0.0 {
      self.x/=l;
      self.y/=l;
      self.z/=l;
    } 
  }
  pub fn normalized(self)->Vec3{
    let l=self.length();
    if l!=0.0 {
      vec3!(self.x/l,self.y/l,self.z/l)
    }else{
      self
    }
  }
  pub fn project(self, v:&Vec3) ->Vec3{
    let b :f32= v.x * v.x + v.y * v.y + v.z * v.z;
    if b != 0.0 {
      let d :f32= (self.x * v.x + self.y * v.y + self.z * v.z) / b;
      vec3!(v.x * d, v.y * d, v.z * d)
    } else {
      vec3!(0.0,0.0,0.0)
    }
  }
  pub fn mul(&self,m:&Matrix)->Vec3{
    vec3!(
      self.x*m.0[0][0] + self.y*m.0[1][0] + self.z*m.0[2][0]  + m.0[3][0],
      self.x*m.0[0][1] + self.y*m.0[1][1] + self.z*m.0[2][1]  + m.0[3][1],
      self.x*m.0[0][2] + self.y*m.0[1][2] + self.z*m.0[2][2]  + m.0[3][2]
    )
  }
}


impl ops::Add for Vec3{
  type Output = Vec3;
  fn add(self,other:Vec3)->Vec3{
    Self::add(&self,&other)
  }
}

impl ops::AddAssign for Vec3 {
  fn add_assign(&mut self, other: Vec3) {
      *self = Self::add(&self,&other)
  }
}

impl ops::Sub for Vec3{
  type Output = Vec3;
  fn sub(self,other:Self)->Vec3{
    Self::sub(&self,&other)
  }
}

impl ops::SubAssign for Vec3 {
  fn sub_assign(&mut self, other: Vec3) {
      *self = Self::sub(&self,&other)
  }
}

//impl ops::Mul<Vec3> for Vec3{
//  type Output = Vec3;
//  fn mul(self,other:Vec3)->Vec3{
//    Self::cross(&self,&other)
//  }
//}

impl ops::Mul<Matrix> for Vec3{
  type Output = Vec3;
  fn mul(self,other:Matrix)->Vec3{
    Self::mul(&self,&other)
  }
}
impl ops::MulAssign<Matrix> for Vec3{
  fn mul_assign(&mut self,other:Matrix){
    *self =Self::mul(&self,&other)
  }
}

//impl ops::MulAssign<Vec3> for Vec3 {
//  fn mul_assign(&mut self, other: Vec3) {
//      *self = Self::cross(&self,&other)
//  }
//}

impl ops::Mul<f32> for Vec3{
  type Output = Vec3;
  fn mul(self,other:f32)->Vec3{
    Self::scalar(&self, other)
  }
}

impl ops::MulAssign<f32> for Vec3 {
  fn mul_assign(&mut self, other: f32) {
      *self = Self::scalar(&self, other)
  }
}

impl ops::Div<f32> for Vec3{
  type Output = Vec3;
  fn div(self,other:f32)->Vec3{
    Self::scalar(&self, 1.0/other)
  }
}

impl ops::DivAssign<f32> for Vec3 {
  fn div_assign(&mut self, other: f32) {
      *self = Self::scalar(&self, 1.0/other)
  }
}

impl ops::Neg for Vec3{
  type Output = Vec3;
  fn neg(self) -> Self::Output {
      vec3!(-self.x,-self.y,-self.z)
  }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        assert_eq!(Vec3::add(&vec3!(1.0,1.0,1.0), &vec3!(2.0,2.0,2.0)), vec3!(3.0,3.0,3.0));
    }
    #[test]
    fn test_add_op() {
        assert_eq!(vec3!(1.0,1.0,1.0)+vec3!(2.0,2.0,2.0), vec3!(3.0,3.0,3.0));
    }
    #[test]
    fn test_sub() {
        assert_eq!(Vec3::sub(&vec3!(1.0,1.0,1.0), &vec3!(2.0,2.0,2.0)), vec3!(-1.0,-1.0,-1.0));
    }
    #[test]
    fn test_cross() {
        assert_eq!(Vec3::cross(&vec3!(1.0,0.0,0.0), &vec3!(0.0,1.0,0.0)), vec3!(0.0,0.0,1.0));
    }
    #[test]
    fn test_scalar() {
        assert_eq!(Vec3::scalar(&vec3!(1.0,1.0,1.0), 3.0), vec3!(3.0,3.0,3.0));
    }
    #[test]
    fn test_scalar_op() {
        assert_eq!(vec3!(1.0,1.0,1.0)* 3.0, vec3!(3.0,3.0,3.0));
    }
    #[test]
    fn test_dot() {
        assert_eq!(Vec3::dot(&vec3!(1.0,1.0,1.0), &vec3!(1.0,1.0,1.0)), 3.0);
    }
    #[test]
    fn test_length() {
        assert_eq!(vec3!(2.0,2.0,1.0).length(), 3.0);
    }
    #[test]
    fn test_normalize() {
        let mut v=vec3!(2.0,2.0,2.0);
        v.normalize();
        assert!(v.length()>0.99999991 && v.length() < 1.0000001 )
    }
    #[test]
    fn test_normalized() {
        let v=vec3!(2.0,2.0,2.0).normalized();
        assert!(v.length()>0.99999991 && v.length() < 1.0000001 )
    }
    #[test]
    fn test_is_zero() {
      assert!(vec3!(0.0,0.0,0.0).is_zero());
    }
}