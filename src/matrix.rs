use std::ops;
use std::f32;
use std::fmt;
use vector3::Vec3;
use vector4::Vec4;

use crate::angle::Rad;

//pub const  TO_RADIANS:f32 = 0.0174532925199433;
//pub const  TO_DEGREES:f32 = 57.29577951308232;
#[repr(C)]
#[derive(Copy, Clone,Debug,PartialEq)]
pub struct Matrix(pub [[f32; 4]; 4]);


impl fmt::Display for Matrix {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "col1 {:?}\ncol2 {:?}\ncol3 {:?}\ncol4 {:?}", self.0[0],self.0[1],self.0[2],self.0[3])
  }
}

impl Matrix{ 

  pub fn new(m:[[f32; 4]; 4])->Matrix{
    Matrix(m)
  }
  
  pub fn identity()->Matrix{
    Matrix([
      [1.0,0.0,0.0,0.0],
      [0.0,1.0,0.0,0.0],
      [0.0,0.0,1.0,0.0],
      [0.0,0.0,0.0,1.0],
    ])
  }

  pub fn zero()->Matrix{
    Matrix([
      [0.0,0.0,0.0,0.0],
      [0.0,0.0,0.0,0.0],
      [0.0,0.0,0.0,0.0],
      [0.0,0.0,0.0,0.0],
    ])
  }

  pub fn translation(tx:f32,ty:f32,tz:f32)->Matrix{
    Matrix([
      [1.0,0.0,0.0,0.0],
      [0.0,1.0,0.0,0.0],
      [0.0,0.0,1.0,0.0],
      [tx,ty,tz,1.0],
    ])
  }

  pub fn translation_from_v3(t:&Vec3)->Matrix{
    Matrix([
      [1.0,0.0,0.0,0.0],
      [0.0,1.0,0.0,0.0],
      [0.0,0.0,1.0,0.0],
      [t.x,t.y,t.z,1.0],
    ])
  }

  pub fn scale(sx:f32,sy:f32,sz:f32)->Matrix{
    Matrix([
      [sx ,0.0,0.0,0.0],
      [0.0,sy ,0.0,0.0],
      [0.0,0.0,sz ,0.0],
      [0.0,0.0,0.0,1.0],
    ])
  }

  pub fn rotate_x(theta:Rad)->Matrix{
    let sin=theta.0.sin();
    let cos=theta.0.cos();
    Matrix([
      [1.0, 0.0,0.0,0.0],
      [0.0, cos,sin,0.0],
      [0.0,-sin,cos,0.0],
      [0.0, 0.0,0.0,1.0],
    ])
  }

  pub fn rotate_y(theta:Rad)->Matrix{
    let sin=theta.0.sin();
    let cos=theta.0.cos();
    Matrix([
      [ cos,0.0,-sin,0.0],
      [ 0.0,1.0,0.0,0.0],
      [ sin,0.0,cos,0.0],
      [ 0.0,0.0,0.0,1.0],
    ])
  }

  pub fn rotate_z(theta:Rad)->Matrix{
    let sin=theta.0.sin();
    let cos=theta.0.cos();
    Matrix([
      [ cos,sin,0.0,0.0],
      [-sin,cos,0.0,0.0],
      [ 0.0,0.0,1.0,0.0],
      [ 0.0,0.0,0.0,1.0],
    ])
  }

  pub fn transposed(m:Matrix)->Matrix{
    Matrix([
      [m.0[0][0],m.0[1][0],m.0[2][0],m.0[3][0]],
      [m.0[0][1],m.0[1][1],m.0[2][1],m.0[3][1]],
      [m.0[0][2],m.0[1][2],m.0[2][2],m.0[3][2]],
      [m.0[0][3],m.0[1][3],m.0[2][3],m.0[3][3]],
    ])
  }

  pub fn mul(m1:&Matrix,m2:&Matrix)->Matrix{
    Matrix([
      [//col0
        m1.0[0][0] * m2.0[0][0] + m1.0[1][0] * m2.0[0][1] + m1.0[2][0] * m2.0[0][2] + m1.0[3][0] * m2.0[0][3],
        m1.0[0][1] * m2.0[0][0] + m1.0[1][1] * m2.0[0][1] + m1.0[2][1] * m2.0[0][2] + m1.0[3][1] * m2.0[0][3],
        m1.0[0][2] * m2.0[0][0] + m1.0[1][2] * m2.0[0][1] + m1.0[2][2] * m2.0[0][2] + m1.0[3][2] * m2.0[0][3],
        m1.0[0][3] * m2.0[0][0] + m1.0[1][3] * m2.0[0][1] + m1.0[2][3] * m2.0[0][2] + m1.0[3][3] * m2.0[0][3],
      ],
      [//col1
        m1.0[0][0] * m2.0[1][0] + m1.0[1][0] * m2.0[1][1] + m1.0[2][0] * m2.0[1][2] + m1.0[3][0] * m2.0[1][3],
        m1.0[0][1] * m2.0[1][0] + m1.0[1][1] * m2.0[1][1] + m1.0[2][1] * m2.0[1][2] + m1.0[3][1] * m2.0[1][3],
        m1.0[0][2] * m2.0[1][0] + m1.0[1][2] * m2.0[1][1] + m1.0[2][2] * m2.0[1][2] + m1.0[3][2] * m2.0[1][3],
        m1.0[0][3] * m2.0[1][0] + m1.0[1][3] * m2.0[1][1] + m1.0[2][3] * m2.0[1][2] + m1.0[3][3] * m2.0[1][3],
      ],
      [//col2
        m1.0[0][0] * m2.0[2][0] + m1.0[1][0] * m2.0[2][1] + m1.0[2][0] * m2.0[2][2] + m1.0[3][0] * m2.0[2][3],
        m1.0[0][1] * m2.0[2][0] + m1.0[1][1] * m2.0[2][1] + m1.0[2][1] * m2.0[2][2] + m1.0[3][1] * m2.0[2][3],
        m1.0[0][2] * m2.0[2][0] + m1.0[1][2] * m2.0[2][1] + m1.0[2][2] * m2.0[2][2] + m1.0[3][2] * m2.0[2][3],
        m1.0[0][3] * m2.0[2][0] + m1.0[1][3] * m2.0[2][1] + m1.0[2][3] * m2.0[2][2] + m1.0[3][3] * m2.0[2][3],
      ],
      [//col3
        m1.0[0][0] * m2.0[3][0] + m1.0[1][0] * m2.0[3][1] + m1.0[2][0] * m2.0[3][2] + m1.0[3][0] * m2.0[3][3],
        m1.0[0][1] * m2.0[3][0] + m1.0[1][1] * m2.0[3][1] + m1.0[2][1] * m2.0[3][2] + m1.0[3][1] * m2.0[3][3],
        m1.0[0][2] * m2.0[3][0] + m1.0[1][2] * m2.0[3][1] + m1.0[2][2] * m2.0[3][2] + m1.0[3][2] * m2.0[3][3],
        m1.0[0][3] * m2.0[3][0] + m1.0[1][3] * m2.0[3][1] + m1.0[2][3] * m2.0[3][2] + m1.0[3][3] * m2.0[3][3],
      ],
    ])
  }
  pub fn invert(&self)->Option<Matrix>{    
      let m00 = self.0[0][0];
      let m01 = self.0[0][1];
      let m02 = self.0[0][2];
      let m03 = self.0[0][3];
      let m10 = self.0[1][0];
      let m11 = self.0[1][1];
      let m12 = self.0[1][2];
      let m13 = self.0[1][3];
      let m20 = self.0[2][0];
      let m21 = self.0[2][1];
      let m22 = self.0[2][2];
      let m23 = self.0[2][3];
      let m30 = self.0[3][0];
      let m31 = self.0[3][1];
      let m32 = self.0[3][2];
      let m33 = self.0[3][3];
      let d00 = m11*m22*m33 + m12*m23*m31 + m13*m21*m32 - m31*m22*m13 - m32*m23*m11 - m33*m21*m12;
      let d01 = m10*m22*m33 + m12*m23*m30 + m13*m20*m32 - m30*m22*m13 - m32*m23*m10 - m33*m20*m12;
      let d02 = m10*m21*m33 + m11*m23*m30 + m13*m20*m31 - m30*m21*m13 - m31*m23*m10 - m33*m20*m11;
      let d03 = m10*m21*m32 + m11*m22*m30 + m12*m20*m31 - m30*m21*m12 - m31*m22*m10 - m32*m20*m11;
      
      let det = m00*d00 - m01*d01 + m02*d02 - m03*d03;
      
      if det == 0.0 {
        return None;
      }

      let d10 = m01*m22*m33 + m02*m23*m31 + m03*m21*m32 - m31*m22*m03 - m32*m23*m01 - m33*m21*m02;
      let d11 = m00*m22*m33 + m02*m23*m30 + m03*m20*m32 - m30*m22*m03 - m32*m23*m00 - m33*m20*m02;
      let d12 = m00*m21*m33 + m01*m23*m30 + m03*m20*m31 - m30*m21*m03 - m31*m23*m00 - m33*m20*m01;
      let d13 = m00*m21*m32 + m01*m22*m30 + m02*m20*m31 - m30*m21*m02 - m31*m22*m00 - m32*m20*m01;

      let d20 = m01*m12*m33 + m02*m13*m31 + m03*m11*m32 - m31*m12*m03 - m32*m13*m01 - m33*m11*m02;
      let d21 = m00*m12*m33 + m02*m13*m30 + m03*m10*m32 - m30*m12*m03 - m32*m13*m00 - m33*m10*m02;
      let d22 = m00*m11*m33 + m01*m13*m30 + m03*m10*m31 - m30*m11*m03 - m31*m13*m00 - m33*m10*m01;
      let d23 = m00*m11*m32 + m01*m12*m30 + m02*m10*m31 - m30*m11*m02 - m31*m12*m00 - m32*m10*m01;

      let d30 = m01*m12*m23 + m02*m13*m21 + m03*m11*m22 - m21*m12*m03 - m22*m13*m01 - m23*m11*m02;
      let d31 = m00*m12*m23 + m02*m13*m20 + m03*m10*m22 - m20*m12*m03 - m22*m13*m00 - m23*m10*m02;
      let d32 = m00*m11*m23 + m01*m13*m20 + m03*m10*m21 - m20*m11*m03 - m21*m13*m00 - m23*m10*m01;
      let d33 = m00*m11*m22 + m01*m12*m20 + m02*m10*m21 - m20*m11*m02 - m21*m12*m00 - m22*m10*m01;


      let mut out=Matrix::identity();
      out.0[0][0] = d00 / det;
      out.0[0][1] = -d10 / det;
      out.0[0][2] = d20 / det;
      out.0[0][3] = -d30 / det;

      out.0[1][0] = -d01 / det;
      out.0[1][1] = d11 / det;
      out.0[1][2] = -d21 / det;
      out.0[1][3] = d31 / det;

      out.0[2][0] = d02 / det;
      out.0[2][1] = -d12 / det;
      out.0[2][2] = d22 / det;
      out.0[2][3] = -d32 / det;

      out.0[3][0] = -d03 / det;
      out.0[3][1] = d13 / det;
      out.0[3][2] = -d23 / det;
      out.0[3][3] = d33 / det;

      Some(out)
    }
  pub fn look_at(eye:&Vec3,target:&Vec3,up:&Vec3) -> Matrix {
      
    let mut z =Vec3::sub(eye, target);
    z.normalize();
    let mut x =Vec3::cross(up,&z);
    x.normalize();
    let mut y = Vec3::cross(&z,&x);
    y.normalize();
    Matrix([
      [x.x,y.x,z.x,0.0],
      [x.y,y.y,z.y,0.0],
      [x.z,y.z,z.z,0.0],
      [-Vec3::dot(&x,eye),-Vec3::dot(&y,eye),-Vec3::dot(&z,eye),1.0],
    ])
  }

  pub fn look_to_rh(eye:&Vec3,dir:&Vec3,up:&Vec3) -> Matrix {
      
    let     z = dir.normalized();
    let mut x = Vec3::cross(&z,up);
    x.normalize();
    let mut y = Vec3::cross(&x,&z);
    y.normalize();
    Matrix([
      [x.x,y.x,-z.x,0.0],
      [x.y,y.y,-z.y,0.0],
      [x.z,y.z,-z.z,0.0],
      [-Vec3::dot(&x,eye),-Vec3::dot(&y,eye),Vec3::dot(&z,eye),1.0],
    ])
  }

  pub fn frustum(l:f32,r:f32,b:f32,t:f32,near:f32,far:f32) -> Matrix {
    let n2 = 2.0*near;
    Matrix([
      [n2/(r-l)    ,0.0        , 0.0                      , 0.0 ],
      [0.0         ,n2/(t-b)   , (t+b)/(t-b)              , 0.0 ],
      [(r+l)/(r-l) ,(t+b)/(t-b), (-(far+near))/(far-near) , -1.0],
      [0.0         ,0.0        , (-n2*far)/(far-near)     , 0.0 ]
    ])
  }

  pub fn ortho(l:f32,r:f32,b:f32,t:f32,near:f32,far:f32) -> Matrix {
    Matrix([
      [2.0/(r-l)      ,0.0            ,0.0                     ,0.0],
      [0.0            ,2.0/(t-b)      ,0.0                     ,0.0],
      [0.0            ,0.0            ,-(2.0/(far-near))       ,0.0],
      [-((r+l)/(r-l)) ,-((t+b)/(t-b)) ,-((far+near)/(far-near)),1.0]
    ])
  }

  pub fn perspective(fov:Rad,aspect:f32,near:f32,far:f32) -> Matrix {

    let ymax = near * (fov.0*0.5).tan();
    let ymin = -ymax;
	  let	xmin = ymin * aspect;
	  let xmax = ymax * aspect;
    Self::frustum(xmin, xmax, ymin, ymax, near, far)
  }
}

impl ops::Mul<Vec3> for Matrix{
  type Output = Vec3;
  fn mul(self,v:Vec3)->Vec3{
    Vec3::mul(&v,&self)
  }
}

impl ops::Mul<Vec4> for Matrix{
  type Output = Vec4;
  fn mul(self,v:Vec4)->Vec4{
    Vec4::mul(&v,&self)
  }
}

impl ops::Mul<Matrix> for Matrix{
  type Output = Matrix;
  fn mul(self,m2:Matrix)->Matrix{
    Self::mul(&self,&m2)
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_translate_vec4() {
      let m=Matrix::translation(2.0, 2.0, 2.0);
      let v=&vec4!(1.0,1.0,1.0,1.0);
      assert_eq!(Vec4::mul(&v,&m), vec4!(3.0,3.0,3.0,1.0));
  }
  #[test]
  fn test_translate_vec3() {
    let m=Matrix::translation(2.0, 3.0, 4.0);
    let v=&vec3!(1.0,2.0,3.0);
    assert_eq!(Vec3::mul(&v,&m), vec3!(3.0,5.0,7.0));
}
  #[test]
  fn test_scale() {
      let m=Matrix::scale(2.0, 3.0, 4.0);
      let v=&vec4!(2.0,2.0,2.0,1.0);
      assert_eq!(Vec4::mul(&v,&m), vec4!(4.0,6.0,8.0,1.0));
  }
  #[test]
  fn test_mul() {
      let m1=Matrix::scale(2.0, 3.0, 4.0);
      let m2=Matrix::translation(5.0, 6.0, 7.0);
      let m3=Matrix::mul(&m2,&m1);
      
      assert_eq!(
        m3, 
        Matrix([
          [2.0,0.0,0.0,0.0],
          [0.0,3.0,0.0,0.0],
          [0.0,0.0,4.0,0.0],
          [5.0,6.0,7.0,1.0]
        ])
      );
  }
  #[test]
  fn test_inv() {
      let m1=Matrix::scale(2.0, 3.0, 4.0);
      let m2=Matrix::translation(5.0, 6.0, 7.0);
      let m3=Matrix::mul(&m2,&m1);
      
      if let Some(m3_inv)=m3.invert(){

        let v=vec4!(2.0,3.0,4.0,1.0);
        let v1=v*m3;
        let v2=v1*m3_inv;
        assert_eq!(v,v2);
      }else{
        panic!("matrix is non invertible");
      }
  }
}